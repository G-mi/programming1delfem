/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class DogManager {

    public DogManager() {
    }

    private static ArrayList<Dog> dogList = new ArrayList<Dog>();

//    public static Dog dog1 = new Dog("A", "B", 10, 10);
//    public static Dog dog2 = new Dog("B", "C", 20, 20);
//    public static Dog dog3 = new Dog("D", "E", 30, 30);
//
//    public void defaultDogs() {
//
//        dogList.add(dog1);
//        dogList.add(dog2);
//        dogList.add(dog3);
//    }


    public void removeDog() {

        if (emptyList()) {
            System.out.println("Error: no dogs in register");
        }

        else {
            Dog dog = null;

            String dogName = enterDogName();

            if (dogInList(dogName)) {
                dog = getDog(dogName);
                String actualName = dog.getName();
                System.out.println(String.format("%s is removed from the register", actualName));
                dogList.remove(dog);
                /// FIX THIS


            } else System.out.println("Error: no such dog");
        }
    }

    private String enterDogName() {
        Scanning input = new Scanning();
        System.out.print("Enter the name of the dog?> ");
        return input.readString();
    }

    public void increaseDogsAge() {

        Dog dog = null;

        String dogName = enterDogName();

        if (dogInList(dogName)) {
            dog = getDog(dogName);
            dog.increaseAge();
            System.out.println(String.format("%s is now one year older", dogName));

        } else System.out.println("Error: no such dog");


    }

    private boolean emptyList() {
        return dogList.size() <= 0;
    }

    public void listDogs() {
        if (emptyList()) {
            System.out.println("Error: no dogs in register");
        }
        else {
            ArrayList<Dog> dogsMatched = new ArrayList<Dog>();
            double shortestTail = readShortestTail();
            for (Dog dog: dogList) {
                if (dog.getTailLength() >= shortestTail) {
                    dogsMatched.add(dog);
                }
            }
            if (dogsMatched.size() == 0) {
                System.out.println(String.format("Error: no dogs with tail length %.2f registered", shortestTail));
            }
            else {
                new DogSorter().sort(dogsMatched);
                for (Dog dog: dogsMatched) {
                    System.out.println(dog);
                }
            }
        }
    }

    private double readShortestTail() {
        Scanning input = new Scanning();
        System.out.print("Smallest tail length to display?> ");
        return input.readDouble();
    }

    public void registerNewDog() {

        Dog dog = generateDog();
        dogList.add(dog);
        System.out.println(String.format("%s added to register", dog.getName()));

    }

    private Dog generateDog() {

        Dog dog = new Dog(readName(), readBreed(), readAge(), readWeight());

        return dog;
    }

    private String readName() {
        String name = null;
        do {
            Scanning input = new Scanning();
            System.out.print("Name?> ");
            name = input.readString();
        } while (invalidName(name));
        return name;
    }

    private boolean invalidName(String name) {
        if (isAWhiteSpaceName(name)) {
            System.out.println("Error: no white spaces or empty input");
            return true;
        }
        else if (dogInList(name)) {
            System.out.println(String.format("Error: the name %s already exists!", name));
            return true;
        }  else return false;
    }

    private boolean isAWhiteSpaceName(String name) {
        name += name.toLowerCase().replaceAll("\\s", "");
        return name.length() == 0 || name.equals(" ") || name.equals("\t") || name.equals("\n") || name.equals("\r");
    }

    private boolean invalidBreed(String breed) {
        if (isAWhiteSpaceName(breed)) {
            System.out.println("Error: no white spaces or empty input");
            return true;
        }
        else return false;
    }

    private String readBreed() {
        String breed = null;
        do {
            Scanning input = new Scanning();
            System.out.print("Breed?> ");
            breed = input.readString();
        } while (invalidBreed(breed));
        return breed;
    }

    private int readAge() {
        Scanning input = new Scanning();
        System.out.print("Age?> ");
        return input.readInt();
    }

    private int readWeight() {
        Scanning input = new Scanning();
        System.out.print("Weight?> ");
        return input.readInt();
    }

    private Dog getDog(String name) {
        Dog dog = null;
        for (Dog i: dogList) {
            if (i.getName().toLowerCase().replaceAll("\\s", "").equals(name.toLowerCase().replaceAll("\\s", ""))) {
                dog = i;
                break;
            }
        } return dog;
    }

    private boolean nameMatch(String nameFirst, String nameSecond) {
        return nameFirst.toLowerCase().replaceAll("\\s", "").equals(nameSecond.replaceAll("\\s", "").toLowerCase());
    }

    private boolean dogInList(String nameInput) {
        for (Dog dog: dogList) {
            if (nameMatch(dog.getName(), nameInput)) {
                return true;
            }
        } return false;
    }
}