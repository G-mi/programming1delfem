/*
 * @author Dimitrios Mavromatis dima1894
 */

public class Run {

    public static void main(String[] arg) {

//        new DogManager().defaultDogs();

        Run program = new Run();

        program.run();
    }

    private void startUp() {
        System.out.println("Welcome to Dimi's Dog Register!");
    }

    private void commandLoop() {
        String command;
        do {
            command = readCommand();
            handleCommand(command);
        } while (!command.equals("exit"));
    }

    private String readCommand() {
        System.out.print("Command?> ");
        Scanning input = new Scanning();
        String command = input.readString().toLowerCase().replaceAll("\\s", "");

        return command;
    }

    private void handleCommand(String command) {
        switch (command) {
            case "registernewdog":
                new DogManager().registerNewDog();
                break;
            case "removedog":
                new DogManager().removeDog();
                break;
            case "increaseage":
                new DogManager().increaseDogsAge();
                break;
            case "listdogs":
                new DogManager().listDogs();
                break;
            case "exit":
                break;
            default:
                System.out.println("Error, no such command!");
        }
    }

    private void closeDown() {
        System.out.println("Thanks, bye!");
    }


    public void run() {
        startUp();
        commandLoop();
        closeDown();

    }
}