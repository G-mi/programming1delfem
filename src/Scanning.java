/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class Scanning {

    public static final Scanner INPUT = new Scanner(System.in);

    public String readString() {
        String userString = INPUT.nextLine();
        return userString;
    }
    
    public int readInt() {
        int userInt = 0;
        while (true) {
            try {
                userInt = INPUT.nextInt();
                INPUT.nextLine();
                return userInt;
            }
            catch (InputMismatchException e)
            {
                System.out.print("Error, provide integer only\n?> ");

                INPUT.nextLine();
            }
        }
    }

    public double readDouble() {
        double userDouble = 0;
        while (true) {
            try {
                userDouble = INPUT.nextDouble();
                INPUT.nextLine();
                return userDouble;
            }
            catch (InputMismatchException e)
            {
                System.out.print("Error, provide double or integer only\n?> ");

                INPUT.nextLine();
            }
        }
    }
}
