/*
 * @author Dimitrios Mavromatis dima1894
 */

public class Dog {

    private static final double TAX_TAIL_LENGTH = 3.7;

    private static final int TAIL_LENGTH_DIVIDER = 10;

    private static String[] taxNameVariations = {"tax", "dachshund", "mayrakoira", "teckel"};

    private String name;
    private String breed;
    private int age;
    private int weight;
    private double tailLength;
    private boolean isTax;

    public Dog(String name, String breed, int age, int weight) {
        this.name  = name;
        this.breed = breed;
        this.age = age;
        this.weight = weight;
        for (int i = 0; i < taxNameVariations.length; i++) {
            if (taxNameVariations[i].equals(breed.toLowerCase())) {
                this.isTax = true;
                break;
            }
        }
        setTailLength();
    }

    public void increaseAge(int increment) {
        if (increment >= 0) {
            this.age += increment;
            setTailLength();
        } else System.out.println("Error, cannot decrease dog's age!");
    }

    public void increaseAge() {
        this.age ++;
        setTailLength();
    }


    private void setTailLength() {
        if (this.isTax) {
            this.tailLength = TAX_TAIL_LENGTH;
            return;
        }
        this.tailLength = age * ((double)weight / TAIL_LENGTH_DIVIDER);
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public double getTailLength() {
        return tailLength;
    }

    /*
    This should include later printing the owner onf the dogs.
     */

    @Override
    public String toString() {
        return String.format("* %s (%s, %d years, %d kilo, %.2f cm tail)", name, breed, age, weight, tailLength);
    }

    public String toString(String attribute) {
        if (attribute.toLowerCase().equals("tail")) {
            return String.format("%.2f cm tail length, dog's name is %s, of breed %s, of age %d, weighs %d kg, and  is .", tailLength, name, breed, age, weight);
        }
        if (attribute.toLowerCase().equals("name")) {
            return String.format("%s is the dog's name, of breed %s, of age %d, weighs %d kg, and tail length is %.2f cm.", name, breed, age, weight, tailLength);
        }
        else {
            return toString();
        }
    }
}
