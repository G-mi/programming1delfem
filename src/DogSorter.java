/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class DogSorter {

    public DogSorter() {
    }

    public void sort(ArrayList<Dog> dogs) {
        for (int i = 0; i < dogs.size(); i++) {
            int lowIndex = i;
            for (int j = i + 1; j < dogs.size(); j++) {

                if (isSmaller(dogs.get(j), dogs.get(lowIndex))) {
                    lowIndex = j;
                }
            }
            Dog shorterTail = dogs.get(lowIndex);
            dogs.set(lowIndex, dogs.get(i));
            dogs.set(i, shorterTail);
        }
    }

    private boolean isSmaller(Dog dogFirst, Dog dogSecond) {
        if (dogFirst.getTailLength() < dogSecond.getTailLength()) {
            return true;
        }
        return dogFirst.getTailLength() == dogSecond.getTailLength() &&
                smallerName(dogFirst.getName(), dogSecond.getName());
    }

    private void sortByName(ArrayList<Dog> dogs) {
        for (int i = 0; i < dogs.size(); i++) {
            int lowIndex = i;
            for (int j = i + 1; j < dogs.size(); j++) {
                Dog dogFirst = dogs.get(j);
                Dog dogSecond = dogs.get(lowIndex);

                if (dogFirst.getTailLength() == dogSecond.getTailLength() &&
                        smallerName(dogFirst.getName(), dogSecond.getName())) {
                    lowIndex = j;
                }
                Dog lowChar = dogs.get(lowIndex);
                dogs.set(lowIndex, dogs.get(i));
                dogs.set(i, lowChar);
            }
        }
    }

    private boolean smallerName(String nameFirst, String nameSecond){
        char[] nameFirstChars = nameFirst.toLowerCase().toCharArray();
        char[] nameSecondChars = nameSecond.toLowerCase().toCharArray();
        for (int x = 0; x < nameSecond.length() && x < nameFirst.length(); x++) {
            if (nameFirstChars[x] < nameSecondChars[x]) {
                return true;
            } else if (nameFirstChars[x] > nameSecondChars[x]) {
                return false;
            }
        }
        return nameFirst.length() < nameSecond.length();
    }
}